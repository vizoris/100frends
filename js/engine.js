$(function() {


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");

// Ошибка в инпуте
$('.form-item__error').hover(function() {
  $(this).addClass('no-error')
})

// Показать все бренды на странице Каталог
$('.tag-list__last > a').click(function() {
  $(this).fadeOut();
  $(this).parents('.tag-list').addClass('active');
})


// Открыть меню каталога
$('.catalog-btn').hover(function() {
  $(this).children('.catalog-menu').stop(true,true).fadeToggle()
})
// Открыть подменю каталога
$('.catalog-menu__dropdown').hover(function() {
  $(this).children('.catalog-submenu').stop(true,true).fadeToggle()
})

// Показать\скрыть пароль
 $('.pass').click(function() { 
    var temp = document.getElementById("pass");
    if (temp.type === "password") {
        
        temp.type = "text";
    } 
    else { 
        temp.type = "password";
        console.log(temp.type);
    } 
}) 

 $('.pass-repeat').click(function() { 
    var temp = document.getElementById("pass-repeat");
    if (temp.type === "password") { 
        temp.type = "text"; 
    } 
    else { 
        temp.type = "password"; 
    } 
}) 


// открыть меню ЛК в шапке
$('.header-top__toggler > a').click(function() {
  $(this).next('.header-top__window').fadeToggle()
})

// Закрыть выпадающее окно в шапке
$('.close-btn').click(function() {
  $(this).parents('.header-top__window').fadeToggle()
})




// BEGIN of script for .main-products__slider
$('.main-products__slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    fade: true,
    pauseOnHover: false,
    arrows: false,
    dots: true
});


// BEGIN of script for .icon-slider
$('.icon-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: true,
    dots: false,
    responsive: [
    {
      breakpoint: 1279,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
     {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 1,
      }
    }
    ]
});

// BEGIN of script for #product-slider-1
$('#product-slider-1, #product-slider-2').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: false,
    dots: false,
    responsive: [
    {
      breakpoint: 1900,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 1279,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});


// Слайдер брендов
if ($(window).width() <= '1279'){

$('.brands-list').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: false,
    dots: false,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      }
    }
  ]

})

}


// Показать\скрыть сайдбар
$('.sidebar-filter__btn').click(function() {
  $(this).toggleClass('active')
  $('.sidebar').toggleClass('active')
})


// Спрятать\показать фильтры в сайдбаре
$('.sidebar-item__header').click(function() {
  $(this).parent().toggleClass('active')
  $(this).next('.sidebar-item__body').fadeToggle()
})

// Слайдер карточки товара
$('.product-cart__slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: false,
    dots: true
});

// Слайдер отзывов
var reviewsSlider = $('.reviews-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: false,
    dots: false
});
$('.reviews-slider__prev').click(function(){
  $(reviewsSlider).slick("slickNext");
 });
 $('.reviews-slider__next').click(function(){
  $(reviewsSlider).slick("slickPrev");
 });



// Слайдер "Недавно смотрели""
$('.recently-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: false,
    dots: false,
    responsive: [
    {
      breakpoint: 1899,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 1279,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});



// "Есть вопросы?" - подробнее

$('.questions-right__more').click(function() {
  $(this).toggleClass('active');
  $('.questions-right').fadeToggle()
})


// Лайк
$(".like-btn").click(function() {
  $(this).toggleClass('active');
})



// Лайк отзыва
$(".reviews-like button").click(function() {
  $(this).parent('.reviews-like').find('button').removeClass('active')
  $(this).addClass('active')
})


// Состав заказа
$(".order-item__toggler").click(function() {
  $(this).toggleClass('active')
})


// Показать все заказы в таблице
$('.user-table__orders--list .togger').click(function() {
  $(this).toggleClass('active');
  $(this).next('.togger-body').fadeToggle()
})






// Диаграммы
// Тут не хватает опыта в js, сроки поджимают, что бы вспоминать массивы ))

var val1 = $('#circles-1').attr("data-circles-value")
Circles.create({
  id:           'circles-1',
  radius:       47,
  value:        val1,
  maxValue:     100,
  width:        10,
  text:         function(value){return value/20},
  colors:       ['#FFDD00', '#014DA1'],
  duration:     400,
  wrpClass:     'circles-wrp',
  textClass:    'circles-text',
  styleWrapper: true,
  styleText:    true
})

var val2 = $('#circles-2').attr("data-circles-value")
Circles.create({
  id:           'circles-2',
  radius:       47,
  value:        val2,
  maxValue:     100,
  width:        10,
  text:         function(value){return value/20},
  colors:       ['#FFDD00', '#014DA1'],
  duration:     400,
  wrpClass:     'circles-wrp',
  textClass:    'circles-text',
  styleWrapper: true,
  styleText:    true
})

var val3 = $('#circles-3').attr("data-circles-value")
Circles.create({
  id:           'circles-3',
  radius:       47,
  value:        val3,
  maxValue:     100,
  width:        10,
  text:         function(value){return value/20},
  colors:       ['#FFDD00', '#014DA1'],
  duration:     400,
  wrpClass:     'circles-wrp',
  textClass:    'circles-text',
  styleWrapper: true,
  styleText:    true
})

var val4 = $('#circles-4').attr("data-circles-value")
Circles.create({
  id:           'circles-4',
  radius:       47,
  value:        val4,
  maxValue:     100,
  width:        10,
  text:         function(value){return value/20},
  colors:       ['#FFDD00', '#014DA1'],
  duration:     400,
  wrpClass:     'circles-wrp',
  textClass:    'circles-text',
  styleWrapper: true,
  styleText:    true
})



// Показать\скрыть пароль
// сори за костыль)

 $('.pass').click(function() { 
    var temp = document.getElementById("pass");
    if (temp.type === "password") {
        temp.type = "text"; 
    } 
    else { 
        temp.type = "password";
    } 
}) 


 $('.pass-repeat').click(function() { 
    var temp = document.getElementById("pass-repeat");
    if (temp.type === "password") { 
        temp.type = "text"; 
    } 
    else { 
        temp.type = "password"; 
    } 
}) 








// Фиксированное меню при прокрутке страницы вниз
if ($(window).width() >= '768'){
$(window).scroll(function(){
        var sticky = $('.navbar'),
            scroll = $(window).scrollTop();
        if (scroll > 200) {
            sticky.addClass('navbar-fixed');
        } else {
            sticky.removeClass('navbar-fixed');
        };
    });
}



// Фиксированные услуги внизу странцы

$(window).scroll(function(){
        var sticky = $('.selected-services'),
            scroll = $(window).scrollTop() + $(window).height();
            windowHeight = $(document).height();
        if (scroll > windowHeight - 250) {
            sticky.removeClass('fixed');
        } else {
            sticky.addClass('fixed');
        };
    });






// BEGIN of script for .shares-slider
var sharesSlider =  $('.shares-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    fade: true,
    pauseOnHover: false,
    arrows: false,
    dots: true
});
$('.shares-slider__prev').click(function(){
  $(sharesSlider).slick("slickPrev");
});
$('.shares-slider__next').click(function(){
  $(sharesSlider).slick("slickNext");
 });



if ($(window).width() >= '768'){
var windowWidth = $( window ).width();
var containerWidth = $( ".container" ).width();
$('.shares-slider .slick-dots').css("left", (windowWidth-containerWidth )/2 + 70);
}







// Стилизация селектов
$('select').styler();



// УСЛУГИ
// Добавить услугу
$('.btn-add a').click(function() {
  $(this).toggleClass('active');
})
// Поменять текст услуги при наведении

$('.our-services__item').hover(function() {
var currentInfo = $(this).children('.our-services__text').clone();
$('.our-services__description').empty().append(currentInfo);
});


// Добавить услугу в модальном окне
$('.modal-services__item').click(function() {
  $(this).toggleClass('active');
})


// Добавить/убрать адрес в личном кабинете
$('.user-data__address-add').click(function() {
  $(this).parent('.user-data__address').addClass('active');
})
$('.user-data__address--close').click(function() {
  $(this).parent('.user-data__address').removeClass('active');
})








$('.map-strore__item label').click(function() {
  $(this).parents(".map-strore__item").toggleClass('active');

})




//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});

// Форма оплаты ТАБЫ
$('.form-payment__nav  a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.get-goods__wrapper').find('#' + _id);
    $('.form-payment__nav  a').removeClass('active');
    $(this).addClass('active');
    $('.form-payment__wrapper > .tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;
});

// Получение товара ТАБЫ
$('.get-goods a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.get-goods__wrapper').find('#' + _id);
    $('.get-goods a').removeClass('active');
    $(this).addClass('active');
    $('.get-goods__wrapper > .tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;
});

// Список-карта ТАБЫ
$('.order-item__tab a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.list-map__wrapper').find('#' + _id);
    $('.order-item__tab a').removeClass('active');
    $(this).addClass('active');
    $('.list-map__wrapper > .tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;
});


// закрыть окно формы  оплаты
$('.order-item__form--close').click(function() {
  $(this).parent('.order-item__block--form').parent('.tabs-item').removeClass('active');
})




// Слайдер партнеров
$('.partners-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
  ]
});


// Клик на кнопку "Показать еще"
  $('.show-more').click(function() {
    $(this).toggleClass('active');
  })


// BEGIN of script for header submenu
    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });


// Выпадающее меню
if($(window).width() >= 768){

	$('.dropdown').hover(function() {
		$(this).children('a').stop(true,true).toggleClass('active');
		$(this).children(".dropdown-menu").stop(true,true).fadeToggle();
	})

	$('.dropdown-menu > li').hover(function() {
	    $(this).children('ul').toggleClass('active');
	})


}


if($(window).width() < 768){

	$('.navbar-toggle').click(function() {
		$('.navbar-collapse').toggleClass('in');
		$('.navbar-nav').toggleClass('active');
	})

  $('.mobile-toggler').click(function() {
    $(this).toggleClass('active');
    $(this).parent('a').next(".dropdown-menu").stop(true,true).fadeToggle();
  })

}




// FansyBox
 $('.fancybox').fancybox({
  beforeShow : function(){
   this.title =  this.title + " - " + $(this.element).data("caption");
   $('select').styler('refresh');
  }
 });










// Закрыть модальное окно
$('.modal-box__close').click(function() {
  $(this).parent(".modal-box").fadeOut();
})


//  Скролл вверх
$('.scroll-top').on('click', function(e) {
    e.preventDefault();
    $('html,body').animate({
        scrollTop: 0
    }, 700);
});





//Begin of GoogleMaps
  var myCenter=new google.maps.LatLng(55.994680, 92.908666);
  var marker;
  function initialize()
  {
    var mapProp = {
    center:myCenter,
    zoom:16,
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000"},{"lightness":80}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000"},{"lightness":70},{"weight":1.2}]}],
    zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_CENTER
    },
    mapTypeId:google.maps.MapTypeId.ROADMAP
    };
      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
      var marker=new google.maps.Marker({
      position:myCenter,
      icon: "img/marker.png"
      // icon: "../img/marker.png" на  хосте может так рабоать     
      });
    marker.setMap(map);
    var content = document.createElement('div');
    content.innerHTML = '<strong class="maps-caption">Красноярский завод строп</strong>';
    var infowindow = new google.maps.InfoWindow({
     content: content
    });
      google.maps.event.addListener(marker,'click',function() {
        infowindow.open(map, marker);
        map.setCenter(marker.getPosition());
      });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
//End of GoogleMaps




// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range
})